import { v4 as uuidv4 } from "uuid";

export default class HoverEvent {
  constructor(options = {}) {
    this.id = options.id || uuidv4();
    this.calendarId = options.calendarId || "";
    this.title = options.title || "";
    this.description = options.description || "";
    this.location = options.location || "";
    this.startTime = options.startTime || "";
    this.endTime = options.endTime || 0;
    this.attendees = options.attendees || [];
  }
}

// attendee
/*
{
    id,
    email,
    display name,
    organizer, // name or 'self'
    location,
    resource
}
*/

// event time
// {
//     dateTime,
//     timeZone
// }
