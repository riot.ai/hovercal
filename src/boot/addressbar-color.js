import { AddressbarColor } from "quasar";

export default () => {
  AddressbarColor.set("#e7e247");
};
