import * as blockstack from "blockstack";
import { UserSession, AppConfig } from "blockstack";

const scopes = ["store_write", "publish_data"];

const userSession = new UserSession({
  appConfig: new AppConfig(scopes)
});

export { blockstack, userSession };
