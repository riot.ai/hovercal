import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import { userSession, blockstack } from "boot/blockstack";
import merge from "lodash.merge";

const DATAFILE = "events.json";

Vue.use(Vuex);

let state = {
  toolbarHeader: "",
  user: {
    authenticated: false
  },
  days: [
    {
      day: "2020-06-30",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-06-30",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-13",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-03",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-03-30",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-07-03",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-26",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-06-25",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-23",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-24",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-06-27",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-28",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-29",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-06-30",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-06-21",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },
    {
      day: "2020-07-01",
      events: [
        {
          calendarId: 1,
          title: "My Event",
          description: "This is something that I plan on doing on the 30th.",
          location: "Home",
          startTime: "12:00",
          endTime: "1:00",
          duration: 60,
          attendees: []
        },
        {
          day: "2020-07-02",
          calendarId: 1,
          title: "Another Event",
          description: "This is something else",
          location: "Home",
          startTime: "2:00",
          endTime: "2:30",
          duration: 30,
          attendees: []
        }
      ]
    },
    {
      day: "2020-07-04",
      events: [
        {
          calendarId: 1,
          title: "Development",
          description: "Do some development",
          location: "Home",
          startTime: "12:00",
          endTime: "12:30",
          duration: 60,
          attendees: []
        }
      ]
    },

  ],
  calendar: {
    id: 1,
    name: "default",
    color: "orange"
  }
};

let mutations = {
  ADD_EVENT(state, payload) {
    console.log("add event mutation payload: ", payload);
    // expects payload {day: "", events: []}
    let idx = state.days.findIndex(day => day.day === payload.day);
    console.log("add event day: ", state.days[idx]);
    if (idx < 0) {
      console.log("new event: ", payload);
      state.days.push(payload);
    } else {
      console.log("update event: ", payload);
      state.days.slice(idx, 1, payload);
    }
  },
  SET_EVENTS(state, payload) {
    state.days = payload;
  },
  UPDATE_EVENT(state, payload) {
    console.log("update events: ", payload);
  },
  UPDATE_DAY(state, payload) {
    Object.assign(state.days[payload.id], payload.events);
  },
  DELETE_DAY(state, day) {
    Vue.delete(state.notes, day);
  },
  ADD_DAY(state, payload) {
    Vue.set(state.notes, payload.id, payload.note);
  },
  SET_HEADER(state, payload) {
    state.toolbarHeader = payload;
  },
  SET_USER(state, payload) {
    Object.assign(state.user, payload);
    // state.user = payload;
  },
  MERGE_NOTES(state, payload) {
    merge(state.notes, payload);
  }
};

let actions = {
  addEvent(context, payload) {
    // expects payload = {day: "", event: {}}
    let idx = context.state.days.findIndex(day => day.day === payload.day);
    let events = [];
    if (idx < 0) {
      console.log("adding new day");
      events = [payload.event];
    } else {
      console.log("updating existing day with: ", payload);
      events = state.days[idx].events.slice();

      events.push(payload.event);
    }
    context.commit("ADD_EVENT", { day: payload.day, events: events });
  },
  async resetData(context) {
    context.dispatch("setUser", { authenticated: false });
    context.dispatch("setEvents", {});
  },
  async mergeNotes(context, payload) {
    await context.commit("MERGE_NOTES", payload);
  },
  async setUser(context, payload) {
    await context.commit("SET_USER", payload);
  },
  setEvents(context, payload) {
    context.commit("SET_EVENTS", payload);
  },
  updateDay(context, payload) {
    context.commit("UPDATE_DAY", payload);
  },
  deleteDay(context, day) {
    context.commit("DELETE_DAY", day);
  },
  async addDay(context, payload) {
    await context.commit("ADD_DAY", payload);
    context.dispatch("web3Store");
  },
  async web3Store(context) {
    userSession.putFile(DATAFILE, JSON.stringify(context.state.notes));
  },
  async loadBlockstackData(context) {
    userSession
      .getFile(DATAFILE)
      .then(data => {
        let events = JSON.parse(data) || {};
        // context.dispatch("mergeNotes", notes)
        context.dispatch("setEvents", events);
      })
      .catch(err => {
        console.log("Error loading notes: ", err);
        context.dispatch("setEvents", {});
      });
  },
  async initBlockstack(context) {
    let userData = userSession.loadUserData();
    let name,
      avatar,
      address = "";
    try {
      let profile = await blockstack.lookupProfile(userData.username);
      name = profile.name ? profile.name : "My Account";
      avatar = profile.image[0].contentUrl
        ? profile.image[0].contentUrl
        : "statics/images/mike-j-hover.svg";
      address = userData.username ? userData.username : "unknown address";
    } catch (err) {
      console.log("Error loading profile: ", err);
      name = "My Account";
      avatar = "/statics/images/mike-j-hover.svg";
      address = "unknown address";
    }
    let user = {
      type: "blockstack",
      name: name,
      address: address,
      avatar: avatar,
      authenticated: true
    };
    await context.dispatch("setUser", user);
    // context.dispatch("loadBlockstackData");
  }
};

const getters = {
  todaysEvents(state, selectedDay) {
    return state.days.filter(day => {
      day.day === selectedDay;
    });
  },
  eventsByDay(state) {
    return state.days.map;
  }
};
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    plugins: [
      createPersistedState({
        key: "HoverLook",
        paths: ["notes"]
      })
    ],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
